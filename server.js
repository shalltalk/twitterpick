var twit = require('twit');
var express = require('express');
var router = express.Router();
var app = express();
var path = require("path");
var cors = require('cors');
app.use(cors({origin: 'null'}));
app.use(express.static('public'));
var ConfigKey = require('./public/config/configKey.json');
var ConfigEnv = require('./public/config/configEnv.json');
var util = require("./server/util.js");
var port = ConfigEnv.port || 8080;
var host = ConfigEnv.host || 'localhost';

var Twit = new twit({
  consumer_key:         ConfigKey.consumer_key,
  consumer_secret:      ConfigKey.consumer_secret,
  access_token:         ConfigKey.access_token,
  access_token_secret:  ConfigKey.access_token_secret,
  timeout_ms:           60*1000,  // optional HTTP request timeout to apply to all requests.
});


router.get('/', function(req, res) {
  res.sendFile("index.html", {"root": path.join(__dirname, '/public/view')});
});

router.get('/tweets', function(req, res) {
	// free api can get tweets within one week only, and up to 100 at a time
	var toDateOrig = req.query.until;
	var toDate;
	if (!toDateOrig) {
		toDate = new Date();
		toDateOrig = toDate;
	} else {
		toDate = toDateOrig;
	}
	toDate = util.getDaysOff(toDate, 1);// make sure tweets created on toDate is included
	var fromDate = req.query.since;
	if (!fromDate) {
		fromDate = util.getDaysOff(toDate, -7);// one week
	} else {
		fromDate = util.formatDate(fromDate);
	}
	var payload = {
		q: req.query.q,
		count: req.query.count,
		geocode: req.query.geocode,
		since: fromDate,
		until: toDate
	};
    Twit.get('search/tweets', payload, function(err, data, response) {
    	if (err) {
    		res.send('<p>' + JSON.stringify(err) + '</p>');
    	} else if (data) {
			var responseTweets = {};
			var key = 'tweets';
			responseTweets[key] = [];
			data.statuses.forEach(function(tweet){
				var displayInfo = {
					text: tweet.text,
					profile_image_url: tweet.user.profile_image_url,
					created_at: util.formatDateTime(tweet.created_at),
					screen_name: tweet.user.screen_name,
					location: tweet.user.location,
					id_str: tweet.id_str
				}
				responseTweets[key].push(displayInfo);
			});
			res.send(JSON.stringify(responseTweets));
		} else {
			res.send('<p>something is wrong</p>');
		}
	});
});

app.use('/', router);

app.listen(port, host, function(){
	console.log('Tweek server is up running...');
});