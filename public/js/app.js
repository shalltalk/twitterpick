var tweekApp = angular.module('tweekApp', ['ngMaterial', 'ngRoute']);
tweekApp.config(function($routeProvider) {
		$routeProvider
		.when('/', {
			templateUrl: '../view/index.html',
			controller: 'searchController'
		});
	}
);
