var tweekApp = angular.module('tweekApp');
tweekApp.controller('searchController', ['$scope', '$http', function($scope, $http){
	var url = 'http://localhost:8080';
    $scope.tweetsCount = ['10', '20', '50', '100'];
    $scope.showOption = false;
    $scope.maxDate = new Date();
    $scope.minDate = new Date($scope.maxDate);
	$scope.minDate.setDate($scope.maxDate.getDate() - 7);// free api can access tweets up to 7 days ago
    $scope.searchTweets = function(keyword, count) {
		$http.get(url + '/tweets', {
			params: {
				q: $scope.keyword,
				count: $scope.count,
				geocode: $scope.searchLocation,
				since: $scope.tweetsDateFrom,
				until: $scope.tweetsDateTo
			}
		}).then(function(response) {
		    console.log(response.data.tweets);
		    $scope.tweets = response.data.tweets;
		}, function(err) {
			alert('Sorry something went wrong, please wait and try again :(');
		});
    }
    $scope.toggleSetting = function() {
		$scope.showOption = !$scope.showOption;
    }
}]);