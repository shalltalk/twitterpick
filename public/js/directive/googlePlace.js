var tweekApp = angular.module('tweekApp');
// courtesy of wallawe at https://gist.github.com/VictorBjelkholm/6687484
tweekApp.directive('googlePlace', [ function() {
    return {
        require: 'ngModel',
        scope: {
            ngModel: '=',
            details: '=?'
        },
        link: function(scope, element, attrs, model) {
            var options = {
                types: []
            };

            scope.gPlace = new google.maps.places.Autocomplete(element[0], options);

            google.maps.event.addListener(scope.gPlace, 'place_changed', function() {
                var geoComponents = scope.gPlace.getPlace();
                var latitude = geoComponents.geometry.location.lat();
                var longitude = geoComponents.geometry.location.lng();
                var addressComponents = geoComponents.address_components;

                addressComponents = addressComponents.filter(function(component){
                    switch (component.types[0]) {
                        case "locality": // city
                            return true;
                        case "administrative_area_level_1": // state
                            return true;
                        case "country": // country
                            return true;
                        case "postal_code": // zipcode
                            return true;
                        default:
                            return false;
                    }
                }).map(function(obj) {
                    return obj.long_name;
                });

                addressComponents.push(latitude, longitude);
                console.log(addressComponents);
                scope.$apply(function() {
                    scope.details = addressComponents; // array containing each location component
                    // for twitter search api, default search radius is 10 miles
                    var geoCode = addressComponents[4] + ',' + addressComponents[5] + ',10mi'
                    model.$setViewValue(geoCode);
                });
            });
        }
    };
}]);