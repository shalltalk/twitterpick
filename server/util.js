var util = {};

util.formatDate = function (date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
}

util.formatDateTime = function (date) {
    var d = new Date(date),
        hour = d.getHours(),
        minute = d.getMinutes(),
        second = d.getSeconds();

    return util.formatDate(date) + ' ' + [hour, minute, second].join(':');
}

// days: how many days from current date, can be negative
util.getDaysOff = function (date, days) {
	var d = new Date(date);
	d.setDate(d.getDate() + days);
	return util.formatDate(d);
}

module.exports = util;